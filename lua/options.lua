vim.opt.number        = true
vim.opt.hlsearch      = true
vim.opt.expandtab     = true
vim.opt.tabstop       = 3
vim.opt.softtabstop   = 3
vim.opt.shiftwidth    = 3
vim.opt.colorcolumn   = "100"
-- vim.opt.termguicolors = true
vim.opt.termguicolors = false

vim.cmd ":hi Cursor guifg=green guibg=green"
vim.cmd ":hi Cursor2 guifg=red guibg=red"
vim.cmd ":set guicursor=n-v-c:block-Cursor/lCursor,i-ci-ve:ver10-Cursor2/lCursor2,r-cr:hor20,o:hor50"
