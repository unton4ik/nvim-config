local keyset = vim.keymap.set
local opts = {noremap = true, silent = true}

keyset('n', '<Leader>', '<Cmd>WhichKey<CR>', opts)
