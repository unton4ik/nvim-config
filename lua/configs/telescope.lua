local keyset = vim.keymap.set
local opts = { noremap = true, silent = true }

keyset('n', '<Leader>km', '<Cmd>Telescope keymaps<CR>',     opts)
keyset('n', '<Leader>cs', '<Cmd>Telescope colorscheme<CR>', opts)
keyset('n', '<Leader>lg', '<Cmd>Telescope live_grep<CR>',   opts)

-- require'telescope'.load_extension('live_grep_args').setup {
--    extensions = {
--       live_grep_args = {
--          smart_case = true,
--       },
--    },
-- }
