vim.opt.updatetime = 300
vim.opt.signcolumn = 'yes'

local keyset = vim.keymap.set
local opts = {noremap = true, silent = true, expr = true, replace_keycodes = false}

keyset('i', '<Tab>',  [[coc#pum#visible() ? coc#pum#confirm() : '<Tab>']], opts)
keyset('i', '<Up>',   [[coc#pum#visible() ? coc#pum#prev(1) : '<C-o>gk']],    opts)
keyset('i', '<Down>', [[coc#pum#visible() ? coc#pum#next(1) : '<C-o>gj']],  opts)

keyset("n", "gd", "<Plug>(coc-definition)",      {silent = true})
keyset("n", "gy", "<Plug>(coc-type-definition)", {silent = true})
keyset("n", "gi", "<Plug>(coc-implementation)",  {silent = true})
keyset("n", "gr", "<Plug>(coc-references)",      {silent = true})

keyset('n', '<Leader>rn', '<Plug>(coc-rename)', {silent = true})
