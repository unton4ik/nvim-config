require'nvim-tree'.setup {
   view = {
      width = 30,
   },
   filters = {
      dotfiles = false,
      git_ignored = false,
   },
}

local keyset = vim.keymap.set
local opts = { silent = true, nowait = true }

keyset('n', '<Leader>n', '<Cmd>NvimTreeToggle<CR>', opts)
