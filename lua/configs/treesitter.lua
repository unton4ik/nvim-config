vim.o.foldmethod = "expr"
vim.o.foldexpr = "nvim_treesitter#foldexpr()"
vim.g.nofoldenable = true

local parser_config = require 'nvim-treesitter.parsers'.get_parser_configs()

parser_config.typst = {
   install_info = {
      url = '~/gits/tree-sitter-typst',
      files = 'src/parser.c',
      branch = 'main',
   },
   filetype = 'typ',
}

parser_config.vhdl = {
   install_info = {
      url = '~/gits/tree-sitter-vhdl',
      files = 'src/parser.c',
      branch = 'main',
   }
}
