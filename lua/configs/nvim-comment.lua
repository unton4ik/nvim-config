local keyset = vim.keymap.set
local opts = {noremap = true, silent = true}

require'nvim_comment'.setup()
keyset('n', '<leader>/', '<Cmd>CommentToggle<CR>', opts)
keyset('v', '<leader>/', ':\'<,\'>CommentToggle<CR>', opts)
