local plugins = {
   {
      'nvim-tree/nvim-tree.lua',
      lazy = false,
      config = (function() require 'configs.nvim-tree' end)
   },
   {
      'folke/which-key.nvim',
      lazy = false,
      config = (function() require 'configs.which-key' end)
   },
   {
      'neoclide/coc.nvim',
      event = 'BufEnter',
      config = (function() require 'configs.coc' end)
   },
   {
      'mg979/vim-visual-multi',
      config = (function() require 'configs.vim-visual-multi' end)
   },
   {
      'tpope/vim-sleuth',
   },
   {
      'romgrk/barbar.nvim',
      dependencies = {
         'lewis6991/gitsigns.nvim',
         'nvim-tree/nvim-web-devicons',
      },
      config = (function() require 'configs.barbar' end)
   },
   {
      'lervag/vimtex',
      config = (function () require 'configs.vimtex' end)
   },
   {
      'nvim-telescope/telescope.nvim',
      dependencies = {
         'nvim-lua/plenary.nvim',
         'nvim-telescope/telescope-live-grep-args.nvim',
      },
      config = (function() require 'configs.telescope' end)
   },
   {
      'nvim-treesitter/nvim-treesitter',
      config = (function () require 'configs.treesitter' end)
   },
   {
      'terrortylor/nvim-comment',
      config = (function() require 'configs.nvim-comment' end)
   },
   {
      'kaarmu/typst.vim',
      ft = 'typst',
      lazy=false
   }
}

return plugins
