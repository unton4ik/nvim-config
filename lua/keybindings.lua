local keyset = vim.keymap.set
local opts = {noremap = false, silent = true}

keyset('n', '<Up>',   'gk',      opts)
keyset('n', '<Down>', 'gj',      opts)
keyset('i', '<Up>',   '<C-o>gk', opts)
keyset('i', '<Down>', '<C-o>gj', opts)

keyset('n', '<leader>h', '<Cmd>50 split<CR><C-w><Down><Cmd>terminal<CR><Cmd>set nonumber<CR>i', {noremap = false, silent = false})
-- keyset('n', '<leader>h', '<Cmd>50 split<CR>', opts)
print('done')
